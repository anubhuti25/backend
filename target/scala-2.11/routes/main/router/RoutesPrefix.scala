
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/welcome/Desktop/Backend1/conf/routes
// @DATE:Sat Mar 11 16:09:40 IST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
